Extracting the muon trace with Neural Networks


The process is divided in two

1. Getting the TTrees from the ADSTs
2. Predicting for these TTrees


## Step 1:  Getting the TTrees from the ADSTs

This step requires having ROOT and Offline installed.

In the folder `trees`, there is an offline program `selection.cc` for
extracting general TTrees from the ADSTs with a lot of information. Make a text
file called sel.txt (for example) with the following information
```
name of the output file 
starting id
path to ADST (one in each line)
```

The starting id can be any positive integer number and has to be different for
different datasets since it is used later to group stations from each event.

An example of the file `sel.txt` could be

```
epos-proton-napoli
1
/DataVol/Simulations/epos-1500-v3r3p4-icrc2017-preprod-v3-napoli/sd/proton/EPOSLHC_185_190_proton_Napoli_Run02.root
/DataVol/Simulations/epos-1500-v3r3p4-icrc2017-preprod-v3-napoli/sd/proton/EPOSLHC_200_202_proton_Napoli_Run04.root
```

If you are using simulations make sure that the line `#DEFINE SIMULATIONS` in
`selection.cc` is uncommented, otherwise make sure that it is commented out.

To compile and run the program (in the folder `trees`) run

```
make
cat sel.txt | ./selection
```

Several files will appear in the folder `Output` inside `trees`. One is the
base file with the information of each event and station and the other ones are
traces. Files with the suffix '-pmt' have the traces for each pmt of each station.

Example files are provided so that this step is not necessary


## Step 2

### Installing python and some packages

This step and the following ones do not require having ROOT installed. Python is
needed. Installing it should be very easy, it is included by default in most
linux distributions. Open a terminal and run `python` to check if it is
installed. Latest version is recommended (3.8.x at the time of writing), make
sure it is not python 2.7.x.

A few python packages are needed to run the programs. The easiest way is
probably using pip, a python package installer. Once installed, run (--user can
be removed to install the packages for all users using sudo before pip)

```
pip install --user numpy pandas h5py torch uproot4 matplotlib
```
matplotlib is optional, if intalled a plot is made at the end

### Predict the muon trace

In the main directory run 

```
python run.py basename
```

The basename is the name of the output file that we have used for the TTree, if following the examples: 
```
python run.py epos-proton-napoli
```
