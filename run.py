import argparse
import uproot4 as uproot
import pandas as pd
import numpy as np
import torch
from torch import nn, optim

def root_to_hdf(filepath, filename=None, output_folder=None,
                return_df=False, tree_name='Tree', dtype=None):
    """
    Transform a ROOT tree given by filepath into a pandas DataFrame
    Either a filename is given for the output or an output folder where
    the files will be named the same but with '.root' changed to '.hdf'

    :param filepath: string
    :param filename: string
    :param output_folder: string
    :param return_df: bool
    :param tree_name: name of the tree of the ROOT file
    :param dtype: dtype used for the output pandas DataFrame (by default)

    :returns: DataFrame if return_df is True (and cache is not True) otherwise None
    """

    if filename is None and output_folder is None:
        raise ValueError("A filename or output folder has to be given for the output")

    ind_root = filepath.find('.root')
    ind_slash = filepath.rfind('/')
    base = filepath[ind_slash + 1: ind_root]
    output_name = base + '.hdf'
    if output_folder is not None:
        filename = output_folder + ('/' if output_folder[-1] != '/' else '') +\
            output_name

    upr = uproot.open(filepath)
    upr = upr[upr.keys()[0]]

    data = pd.DataFrame()
    dic = {}
    ary = []
    # If we only saved one array
    # It seems arrays are v9
    if len(upr.items()) == 1 and isinstance(upr.values()[0], uproot.models.TBranch.Model_TBranchElement_v9):
        key = upr.keys()[0]
        tmp = upr.arrays()
        data = pd.DataFrame(np.array(tmp[key]))
    else:
        data = upr.arrays(library='pd')

    data.to_hdf(filename, 'data', format='f', mode='w')
    if return_df:
        return data

def select(df):
    cut = 'r != 0 and s > 5 and saturated == 0'
    df = pd.DataFrame(df.query(cut))
    return df

class PreprocessLSTM:
    """
    Preprocessing class for the prediction with a LSTM station by station
    Returns the traces and several variables
    (r, zenith_mc for simulations and zenith for data) that are used 
    for the initialization of the LSTM parameters
    """
    def _transform(self, data):
        print('Preprocess step 1: ', end='')
        zenith_var = 'zen'
        cols = ['r', zenith_var] +\
            [f't{i}' for i in range(200)] +\
            ([f'tm{i}' for i in range(200)] if 'tm0' in data[0] else [])
        return [df[cols] for df in data]
    def transform(self, data):
        return self._transform(data)
    def fit_transform(self, data):
        return self._transform(data)

class ScalerLSTM:
    """
    Scaling class for the prediction with a LSTM station by station
    Traces are divided by the maximum value of each trace, i.e. the values of
    the bins are between 0 and 1 (and some possible negative values)

    """
    def _transform(self, data):
        print('Preprocess step 2 (scaling the traces): ', end='')
        for df in data:
            df.r /= 4000
            zenith_var = 'zen'
            df[zenith_var] /= 6 # 6 was the original value/

            trace_start = df.columns.get_loc('t0')
            df['scalers'] = df.iloc[:, trace_start:trace_start + 200].max(axis=1).values
            # df['scalers'] = np.full(len(df), 6.0, np.float16)
            df.iloc[:, trace_start:trace_start + 200] =\
                df.iloc[:, trace_start:trace_start + 200].div(df.scalers, axis='index')
            if 'tm0' in df:
                trace_muon_start = df.columns.get_loc('tm0')
                df.iloc[:, trace_muon_start:trace_muon_start + 200] =\
                    df.iloc[:, trace_muon_start:trace_muon_start + 200].div(df.scalers, axis='index')
        return data
    def transform(self, data):
        return self._transform(data)
    def fit_transform(self, data):
        return self._transform(data)

class net(nn.Module):
    def __init__(self):
        super(net, self).__init__()

        self.dense_2_ha = nn.Linear(2, 32)
        self.dense_2_hb = nn.Linear(32, 70)
        self.dense_2_ca = nn.Linear(2, 32)
        self.dense_2_cb = nn.Linear(32, 70)

        self.lstm1 = nn.LSTM(1, 70)
        self.lstm2 = nn.LSTM(70, 32)
        self.lstm3 = nn.LSTM(32, 32)

        self.out1 = nn.Linear(200, 200)
        self.relu = nn.ReLU()

    def forward(self, features, series):
        features_num = 2

        features_h = self.dense_2_ha(features)
        features_h = self.relu(features_h)
        features_h = self.dense_2_hb(features_h)
        features_h = self.relu(features_h)
        features_h = features_h[None, :, :]

        features_c = self.dense_2_ca(features)
        features_c = self.relu(features_c)
        features_c = self.dense_2_cb(features_c)
        features_c = self.relu(features_c)
        features_c = features_c[None, :, :]

        series, hidden = self.lstm1(series, (features_h, features_c))
        series, hidden = self.lstm2(series)
        series, hidden = self.lstm3(series)
        out = series
        out = torch.transpose(out[..., 0], 1, 0)
        out = self.relu(self.out1(out))
        return out

class NeuralNetworkPytorch():
    def __init__(self):
        self.device = torch.device('cpu')
        self.model = net().to(self.device)
        self.optimizer = optim.Adam(self.model.parameters(), lr=1e-3)
        print('Summary of the model')
        print('Number of parameters '
            f'{sum(p.numel() for p in self.model.parameters())}')
        print('Number of trainable parameters '
            f'{sum(p.numel() for p in self.model.parameters() if p.requires_grad)}')

    def predict(self, data):
        df = data[0]
        scalers = df.pop('scalers')
        self.model.eval()
        train = data[0]
        pred_batch_size = 10000
        with torch.no_grad():
            trace_start = train.columns.get_loc('t0')
            features_x = train.iloc[:, :trace_start].values
            series_x = np.transpose(train.iloc[:, trace_start:trace_start + 200].values[:, :, np.newaxis], (1, 0, 2))
            features_x = torch.tensor(features_x, dtype=torch.float32).to(self.device)
            series_x = torch.tensor(series_x, dtype=torch.float32).to(self.device)
            ls = []
            for j in range(0, features_x.shape[0], pred_batch_size):
                out = self.model.forward(features_x[j:j+pred_batch_size], series_x[:, j:j+pred_batch_size])
                ls.append(out)
            out = torch.cat(ls)
        return out.cpu().numpy() * scalers[:, None]

if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Transform ROOT files to HDF files')
    parser.add_argument('files', type=str, nargs='*',
                         help='ROOT files that will be converted to hdf')

    args = parser.parse_args()
    if args.files is None:
        parser.error('files not specified')

    output_folder = 'data/raw'
    files = []
    for f in args.files:
        files.append(f+'.root')
        files.append(f+'-trace.root')

    data = []
    for i, f in enumerate(files):
        data.append(root_to_hdf('trees/output/' + f, output_folder=output_folder, dtype='float32' if i%2==0 else 'float16', return_df=True))

    steps = [PreprocessLSTM, ScalerLSTM, NeuralNetworkPytorch]
    for i in range(len(steps)):
        steps[i] = steps[i]()
    steps[-1].model = torch.load('model/' + 'net.pt', map_location=lambda storage, loc: storage)
    # steps[-1].model = torch.load('model/' + 'net.pt')

    for i in range(0, len(files) // 2, 2):
        columns = ['sim_id', 'r', 'zen', 'zen_mc', 'en_mc', 's', 'saturated']
        if 'muon_signal_mc' in data[i]:
            columns.append('muon_signal_mc')
        df = data[i][columns]
        trace = data[i+1]
        trace.columns = ['t' + str(x) for x in trace.columns]

        mask = pd.isna(trace) | np.isinf(trace)
        mask = np.unique(np.where(mask)[0])
        df.query('index not in @mask', inplace=True)
        trace.query('index not in @mask', inplace=True)

        df = pd.concat((df, trace), axis=1)
        df = select(df)
        

    df = [df]
    muon_signal = df[0].muon_signal_mc
    ndf = df
    for i, step in enumerate(steps): 
        if i != len(steps) - 1:
            ndf = step.transform(ndf)
        else:
            pred = step.predict(ndf)
    dif = pred.sum(axis=1)-muon_signal
    print('Mean of the differences between the predicted integral and the true integral: ', dif.mean())
    print('Standard deviation of the differences between the predicted integral and the true integral: ', dif.std())


    try:
        import matplotlib.pyplot as plt
        plt.hist(dif, bins=np.arange(-5, 5, .5))
        plt.xlabel('$\widehat{S^\mu}-S^\mu$ [VEM]')
        plt.ylabel('Entries')
        plt.show()
    except:
        print('matplotlib is not installed, plot not made')
