import pandas as pd
import joblib
# import tensorflow as tf
import numpy as np
import math
import time
import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
import tqdm
import argparse
from datetime import datetime
from sacred import Experiment, cli_option

epochs = 300
batch_size = 512

fast = False
@cli_option('-f', '--fast', is_flag=True)
def set_fast(args, run):
    global fast
    fast = True

ex = Experiment('Muon prediction', additional_cli_options=[set_fast])

import sys
if not '--fast' in sys.argv:
    from sacred.observers import MongoObserver
    ex.observers.append(MongoObserver())
else:
    epochs = 2


class PreprocessLSTM:
    """
    Preprocessing class for the prediction with a LSTM station by station
    Returns the traces and several variables
    (r, zenith for simulations and zenith for data) that are used 
    for the initialization of the LSTM parameters
    """
    def _transform(self, data):
        print('Preprocess step 1: ', end='')
        time_start = time.process_time()
        zenith_var = 'zen'
        cols = ['r', zenith_var] +\
               [f't{i}' for i in range(200)] +\
               ([f'tm{i}' for i in range(200)] if 'tm0' in data[0] else [])
        print(time.process_time()-time_start)
        return [df[cols] for df in data]
    def transform(self, data):
        return self._transform(data)
    def fit_transform(self, data):
        return self._transform(data)

class ScalerLSTM:
    """
    Scaling class for the prediction with a LSTM station by station
    Traces are divided by the maximum value of each trace, i.e. the values of
    the bins are between 0 and 1 (and some possible negative values)

    """
    def _transform(self, data):
        print('Preprocess step 2 (scaling the traces): ', end='')
        time_start = time.process_time()
        for df in data:
            df.r /= 4000
            zenith_var = 'zen'
            df[zenith_var] /= 6 # 6 was the original value/

            trace_start = df.columns.get_loc('t0')
            df['scalers'] = df.iloc[:, trace_start:trace_start + 200].max(axis=1).values
            # df['scalers'] = np.full(len(df), 6.0, np.float16)
            df.iloc[:, trace_start:trace_start + 200] =\
                df.iloc[:, trace_start:trace_start + 200].div(df.scalers, axis='index')
            if 'tm0' in df:
                trace_muon_start = df.columns.get_loc('tm0')
                df.iloc[:, trace_muon_start:trace_muon_start + 200] =\
                    df.iloc[:, trace_muon_start:trace_muon_start + 200].div(df.scalers, axis='index')
        print(time.process_time()-time_start)
        return data
    def transform(self, data):
        return self._transform(data)
    def fit_transform(self, data):
        return self._transform(data)

class net(nn.Module):
    def __init__(self):
        super(net, self).__init__()
        # features_input = tf.keras.layers.Lambda(lambda x: x[:, :features_num, 0])(inp)

        self.dense_2_ha = nn.Linear(2, 32)
        self.dense_2_hb = nn.Linear(32, 70)
        self.dense_2_ca = nn.Linear(2, 32)
        self.dense_2_cb = nn.Linear(32, 70)

        # self.dense_2_ha_2 = nn.Linear(2, 32)
        # self.dense_2_hb_2 = nn.Linear(32, 200)
        # self.dense_2_ca_2 = nn.Linear(2, 32)
        # self.dense_2_cb_2 = nn.Linear(32, 200)

        # self.rstm1 = nn.LSTM(200, 200, 2, bidirectional=True)
        # self.lstm2 = nn.LSTM(200, 200, bidirectional=True)
        # self.lstm3 = nn.LSTM(200, 200, bidirectional=True)
        self.lstm1 = nn.LSTM(1, 70)
        self.lstm2 = nn.LSTM(70, 32)
        self.lstm3 = nn.LSTM(32, 32)
        # self.lstm4 = nn.LSTM(32, 16)
        # self.lstm5 = nn.LSTM(16, 1)

        self.out1 = nn.Linear(200, 200)
        # self.out2 = nn.Linear(256, 200)
        self.relu = nn.ReLU()

    def forward(self, features, series):
        features_num = 2

        features_h = self.dense_2_ha(features)
        features_h = self.relu(features_h)
        features_h = self.dense_2_hb(features_h)
        features_h = self.relu(features_h)
        features_h = features_h[None, :, :]

        features_c = self.dense_2_ca(features)
        features_c = self.relu(features_c)
        features_c = self.dense_2_cb(features_c)
        features_c = self.relu(features_c)
        features_c = features_c[None, :, :]

        # features_h_2 = self.dense_2_ha_2(features)
        # features_h_2 = self.relu(features_h_2)
        # features_h_2 = self.dense_2_hb_2(features_h_2)
        # features_h_2 = self.relu(features_h_2)
        # features_h_2 = features_h_2[None, :, :]

        # features_c_2 = self.dense_2_ca_2(features)
        # features_c_2 = self.relu(features_c_2)
        # features_c_2 = self.dense_2_cb_2(features_c_2)
        # features_c_2 = self.relu(features_c_2)
        # features_c_2 = features_c_2[None, :, :]

        # temp = self.dense1(features)
        # temp = self.relu(temp)
        # temp= self.dense2(temp)
        # temp= self.relu(temp)
        # features_c = temp[:, 400:].reshape(2, temp.shape[0], 200)
        # features_c = torch.cat([features_c, features_c])
        # features_h = temp[:, :400].reshape(2, temp.shape[0], 200)
        # features_h = torch.cat([features_h, features_h])

        # series = series.permute(2, 0, 1)
        # series = torch.cat([series, series])

        series, hidden = self.lstm1(series, (features_h, features_c))
        series, hidden = self.lstm2(series)
        series, hidden = self.lstm3(series)
        # series, hidden = self.lstm4(series)
        # series, hidden = self.lstm5(series)

        # series, hidden = self.lstm1(series, (torch.cat([features_h, features_h_2,
        #                                                 features_h, features_h_2,
        # ]),
        #                                      torch.cat([features_c, features_c_2,
        #                                                 features_c, features_c_2,
        #                                      ])))

        # series, hidden = self.lstm1(series, (features_h, features_c))

        # series = (series[:, :, :200] + series[:, :, 200:]) / 2

        # series, hidden = self.lstm2(series, (torch.cat([features_h, features_h_2]), torch.cat([features_c, features_c_2])))
        # series = (series[:, :, :200] + series[:, :, 200:]) / 2
        # series, hidden = self.lstm3(series, (torch.cat([features_h, features_h_2]), torch.cat([features_c, features_c_2])))
        # series = (series[:, :, :200] + series[:, :, 200:]) / 2
        out = series
        out = torch.transpose(out[..., 0], 1, 0)
        # size = out.shape[0]
        # out = out.reshape((size, -1))
        out = self.relu(self.out1(out))
        # out = self.relu(self.out2(out))
        # out = self.out(series)
        return out

class NeuralNetworkPytorch():
    def __init__(self):
        self.device = torch.device('cuda')
        self.model = net().to(self.device)
        self.optimizer = optim.Adam(self.model.parameters(), lr=1e-3)
        self.scheduler = optim.lr_scheduler.StepLR(self.optimizer, 30, .9)
        print('Summary of the model')
        print('Number of parameters '
              f'{sum(p.numel() for p in self.model.parameters())}')
        print('Number of trainable parameters '
              f'{sum(p.numel() for p in self.model.parameters() if p.requires_grad)}')

    def fit(self, data):
        train, dev = data
        scalers_train = train.pop('scalers')
        scalers_dev = dev.pop('scalers')
        trace_start = train.columns.get_loc('t0')
        trace_muon_start = train.columns.get_loc('tm0')

        train_y = train.values[:, trace_muon_start:trace_muon_start + 200, np.newaxis]
        features_x = train.iloc[:, :trace_start].values
        series_x = np.transpose(train.iloc[:, trace_start:trace_start + 200].values[:, :, np.newaxis], (1, 0, 2))

        dev_y = dev.values[:, trace_muon_start:trace_muon_start + 200, np.newaxis]
        dev_features_x = dev.iloc[:, :trace_start].values
        dev_series_x = np.transpose(dev.iloc[:, trace_start:trace_start + 200].values[:, :, np.newaxis], (1, 0, 2))
        
        t0 = time.time()
        print('Starting the fit')
        print(features_x.dtype, series_x.dtype)
        print(train_y.dtype)
        # features_x = torch.tensor(features_x, dtype=torch.float32).to(self.device)
        # series_x = torch.tensor(series_x, dtype=torch.float32).to(self.device)
        # train_y = torch.tensor(train_y, dtype=torch.float32).to(self.device)
        # dev_features_x = torch.tensor(dev_features_x, dtype=torch.float32).to(self.device)
        # dev_series_x = torch.tensor(dev_series_x, dtype=torch.float32).to(self.device)
        # dev_y = torch.tensor(dev_y, dtype=torch.float32).to(self.device)
        features_x = torch.tensor(features_x, dtype=torch.float32)
        series_x = torch.tensor(series_x, dtype=torch.float32)
        train_y = torch.tensor(train_y, dtype=torch.float32)
        dev_features_x = torch.tensor(dev_features_x, dtype=torch.float32)
        dev_series_x = torch.tensor(dev_series_x, dtype=torch.float32)
        dev_y = torch.tensor(dev_y, dtype=torch.float32)
        scalers_dev = torch.tensor(scalers_dev.values, dtype=torch.float32).to(self.device)
        name =  'log-' + datetime.now().strftime('%y%m%d-%H%M%S')
        log = open('../../logs/' + name, 'a', buffering=1)
        mean_loss = 0
        for i in range(epochs):
            epoch_time = time.time()
            print(f'Epoch {i+1}/{epochs}')
            permutation = torch.randperm(features_x.size()[0])
            # bar = tqdm.tqdm(range(0, features_x.size()[0], batch_size))
            # for j in range(0, features_x.size()[0], batch_size):
            # forward_time = 0
            # backward_time = 0
            # step_time = 0
            for j in range(0, features_x.size()[0], batch_size):
                indices = permutation[j:j+batch_size]
                features, series, batch_y = features_x[indices].to(self.device), series_x[:, indices].to(self.device), train_y[indices].to(self.device)
                self.optimizer.zero_grad()
                # ft = time.time()
                out = self.model.forward(features, series)
                # forward_time += time.time() - ft
                y = batch_y[:, :, 0]
                train_loss = torch.sqrt(((out - y) ** 2).mean())
                mean_loss = (mean_loss * j + float(train_loss)) / (j+1)
                # print('Training loss', loss)
                # bt = time.time()
                train_loss.backward()
                # backward_time += time.time() - bt
                # st = time.time()
                self.optimizer.step()
                # step_time += time.time() - st
            epoch_time = time.time() - epoch_time
            # print('Epoch time', epoch_time)
            # print('Forward time', forward_time)
            # print('Backward time', backward_time)
            # print('Step time', step_time)
            with torch.no_grad():
                y = dev_y[:, :, 0].to(self.device)
                dev_batch_size = 5000
                ls = []
                for j in range(0, dev_features_x.shape[0], dev_batch_size):
                    out = self.model.forward(dev_features_x[j:j+dev_batch_size].to(self.device), dev_series_x[:, j:j+dev_batch_size].to(self.device))
                    ls.append(out)
                out = torch.cat(ls)
                dev_loss = float(torch.sqrt(((out - y) ** 2).mean()))
                print('Training loss', mean_loss)
                print('Validation loss', dev_loss)
                outs = out.sum(axis=1)
                ys = y.sum(axis=1)
                diff = (outs - ys) * scalers_dev
                mean, std = float(diff.mean()), float(diff.std())
                print(mean, std)
                ex.log_scalar('Training loss', mean_loss)
                ex.log_scalar('Dev loss', dev_loss)
                ex.log_scalar('Mean muon signal', mean)
                ex.log_scalar('Std muon signal', std)
            # self.scheduler.step()
            log.write(' '.join(map(str, [i+1, mean_loss, dev_loss, mean, std, epoch_time])) + '\n')
        training_time = time.time() - t0
        print(f'Total training time {training_time}')
        log.close()
        return mean_loss, dev_loss

    def predict(self, data):
        df = data[0]
        scalers = df.pop('scalers')
        self.model.eval()
        train = data[0]
        pred_batch_size = 10000
        with torch.no_grad():
            trace_start = train.columns.get_loc('t0')
            features_x = train.iloc[:, :trace_start].values
            series_x = np.transpose(train.iloc[:, trace_start:trace_start + 200].values[:, :, np.newaxis], (1, 0, 2))
            features_x = torch.tensor(features_x, dtype=torch.float32).to(self.device)
            series_x = torch.tensor(series_x, dtype=torch.float32).to(self.device)
            ls = []
            for j in range(0, features_x.shape[0], pred_batch_size):
                out = self.model.forward(features_x[j:j+pred_batch_size], series_x[:, j:j+pred_batch_size])
                ls.append(out)
            out = torch.cat(ls)
        return out.cpu().numpy() * scalers[:, None]

# class TraceDataset(Dataset):
#     def __init__(self):
#         self.dataset = 

#     def __len__(self):
#         return len(dataset)

#     def __getitem__(self, idx):
#         return self.dataset[idx]

@ex.automain
def main(_run):

    # parser = argparse.ArgumentParser(description='Change the signals in simulations so that they match the signals in data')
    # parser.add_argument('--fast', help='fast training for debug and testing purposes', action='store_true')
    # args = parser.parse_args()
    

    steps = {
        0: [PreprocessLSTM, ScalerLSTM, NeuralNetworkPytorch],
    }[0]
    for i in range(len(steps)):
        steps[i] = steps[i]()

    print('Reading the training dataset')
    train = (pd.read_hdf('../../data/processed/' + 'epos-napoli-train.hdf')
             [['r', 'zen'] +
              [f't{i}' for i in range(200)] + [f'tm{i}' for i in range(200)]]
             # .sample(frac=.5)
             )
    # train = train.iloc[:int(len(train)*.1)]
    print('Reading the dev dataset')
    dev = (pd.read_hdf('../../data/processed/' + 'epos-napoli-dev.hdf')
           [['r', 'zen'] +
            [f't{i}' for i in range(200)] + [f'tm{i}' for i in range(200)]])
    if fast:
        print('Fast mode enabled, setting the number of rows to 20000'
              ' for the train and dev datasets'
        )
        train = train.sample(n=20000)
        dev = dev.sample(n=20000)
    print('Memory usage of the training dataset: '
          f'{train.memory_usage(deep=True).sum() / 1024 ** 2:.2f} MB')
    print('Memory usage of the dev dataset: '
          f'{dev.memory_usage(deep=True).sum() / 1024 ** 2:.2f} MB')

    for i, step in enumerate(steps): 
        if i != len(steps) - 1:
            train, dev = step.fit_transform([train, dev])
        else:
            data = step.fit([train, dev])

    # Save the model
    torch.save(steps[-1].model, '../../models/' + 'net.pt')
    ex.add_artifact('../../models/' + 'net.pt')
    return float(data[1])

    if 0:
        dev = pd.read_hdf('../../data/processed/' + 'epos-napoli-dev.hdf').query('total_signal__s > 5')
        dev = dev[['sp_distance__s', 'zenith__e'] +
                [f't{i}' for i in range(200)] + [f'tm{i}' for i in range(200)]]
        dev = [dev]
        for i, step in enumerate(steps): 
            if i != len(steps) - 1:
                dev = step.transform(dev)
            else:
                pred = step.predict(dev)
        # pred = pipeline.predict([dev])
        dev = pd.read_hdf('../../data/processed/' + 'epos-napoli-dev.hdf').query('total_signal__s > 5')
        muon_signal = dev.muon_signal_mc__s
        dif = pred.sum(axis=1) - muon_signal
        print(dif.mean(), dif.std())
        print(((pred-dev[[f'tm{i}' for i in range(200)]].values) ** 2).mean())
        
        dif = (pred-dev[[f'tm{i}' for i in range(200)]].values.astype(np.float64))
        print('Second step')
        maxs = dev[[f't{i}' for i in range(200)]].max(axis=1).values.reshape((-1, 1))
        import math
        print( math.sqrt(((dif / maxs)**2).mean() ) )
        input()

