#include "selection.h"
//#include <chrono>
#define TRACE_BINS 200         // number of bins of the trace to save
                               // can be always uncommented
//#define SIMULATION             // uncomment when extracting data from simulations
                               // and you want something that is not in data
//#define UNSATURATED_TRACE      // uncomment if you want to save the unsaturated trace
                               // only works if TRACE and SIMULATION are uncommented

std::vector<double> trace_info (const std::vector<float> &signal, const int start, const int end) {
    //total signal from the trace, risetime, falltime, area over peak
    double trace_signal = 0;
    double trace_peak = 0;
    for (int j = start; j != end; ++j) {
        trace_signal += signal[j];
        trace_peak = std::max(float(trace_peak), signal[j]);
    } 
    double partial_sum = 0;
    int i = start;
    int first_index, second_index;

    while (partial_sum < 0.1 * trace_signal)
        partial_sum += signal[i++];
    first_index = i;
    while (partial_sum < 0.5 * trace_signal)
        partial_sum += signal[i++];
    second_index = i;
    while (partial_sum < 0.9 * trace_signal)
        partial_sum += signal[i++];
    double risetime = 25 * (second_index - first_index);
    double falltime = 25 * (i - second_index);
    
    return {risetime, falltime, trace_signal / trace_peak};
}

// inline double sec (const double angle) {
//     return 1 / cos(angle);
// }

inline double correction (const double seczenith, const double azimuth, const double r)
{
    double a = -3.96e-5, b = -1.94e-5, c = 2.01e-4;
    return - (a * seczenith + b * pow(seczenith, 3) + c) * sqrt(seczenith - 1) * r * r * cos(azimuth);
}

double risetime_error (double r, double seczenith, double signal){
    double p0, p1;
    if (r < 650){
        p0 = -340 + 186 * seczenith;
        p1 = 0.94 - 0.44 * seczenith;
    }
    else{
        p0 = -447 + 224 * seczenith;
        p1 = 1.12 - 0.51 * seczenith;
    }
    return sqrt( pow((p0 + p1 * r), 2) / (signal) + 104.16666666666669);
}


void make_tree(const std::string& name, const std::vector<std::string>& files, const std::string& output, int sim_id){
    std::cout << name << " " << output << " " << sim_id << std::endl;
    std::cin.ignore();

    std::string treename = output + name + ".root";

    RecEvent *theRecEvent = new RecEvent(); 
    FileInfo theInfo;

    TFile *main_file = new TFile(treename.c_str(), "RECREATE"); // Crear archivo de salida y Tree
    TTree *tree = new TTree("Tree", "Tree");

    tree->Branch("sim_id", &sim_id);

    int event_id; tree->Branch("event_id",&event_id);
    int station_id; tree->Branch("station_id", &station_id);

//////////////          TRACES         //////////////

    std::string trace_treename = output + name + "-trace.root";
    TFile *trace_file = new TFile(trace_treename.c_str(), "RECREATE"); 
    TTree *trace_tree = new TTree("Tree", "Tree");
    std::vector<float> trace;
    trace_tree->Branch("trace", &trace);

    std::vector<std::vector<float>> trace_pmt(3, std::vector<float>(200));
    TFile *trace_file_pmt = new TFile((output + name + "-trace-pmt.root").c_str(), "RECREATE");
    TTree *trace_tree_pmt = new TTree("Tree", "Tree");

    for (int i=0; i!=3; ++i)
        trace_tree_pmt->Branch(("trace" + std::to_string(i+1)).c_str(), &trace_pmt[i]);
    
        #ifdef SIMULATION
        std::string trace_muon_treename = output + name + "-trace-muon.root";
        std::string trace_electromagnetic_treename = output + name + "-trace-electromagnetic.root";
        TFile *trace_muon_file = new TFile(trace_muon_treename.c_str(), "RECREATE"); 
        TTree *trace_muon_tree = new TTree("Tree", "Tree");
        TFile *trace_electromagnetic_file = new TFile(trace_electromagnetic_treename.c_str(), "RECREATE"); 
        TTree *trace_electromagnetic_tree = new TTree("Tree", "Tree");
        std::vector<float> trace_muon;
        trace_muon_tree->Branch("trace_muon", &trace_muon);
        std::vector<float> trace_electron;
        std::vector<float> trace_photon;
        std::vector<float> trace_electromagnetic;
        trace_electromagnetic_tree->Branch("trace_electromagnetic", &trace_electromagnetic);

        std::vector<std::vector<float>> trace_muon_pmt(3, std::vector<float>(200));
        TFile *trace_muon_file_pmt = new TFile((output + name + "-trace-muon-pmt.root").c_str(), "RECREATE");
        TTree *trace_muon_tree_pmt = new TTree("Tree", "Tree");
        for (int i=0; i!=3; ++i)
            trace_muon_tree_pmt->Branch(("trace" + std::to_string(i+1)).c_str(), &trace_muon_pmt[i]);

        std::vector<std::vector<float>> trace_electromagnetic_pmt(3, std::vector<float>(200));
        TFile *trace_electromagnetic_file_pmt = new TFile((output + name + "-trace-electromagnetic-pmt.root").c_str(), "RECREATE");
        TTree *trace_electromagnetic_tree_pmt = new TTree("Tree", "Tree");
        for (int i=0; i!=3; ++i)
            trace_electromagnetic_tree_pmt->Branch(("trace" + std::to_string(i+1)).c_str(), &trace_electromagnetic_pmt[i]);


            #ifdef UNSATURATED_TRACE
            std::string trace_unsaturated_treename = output + name + "-trace-unsaturated.root";
            TFile *trace_unsaturated_file = new TFile(trace_unsaturated_treename.c_str(), "RECREATE"); 
            TTree *trace_unsaturated_tree = new TTree("Tree", "Tree");
            std::vector<float> trace_unsaturated;
            trace_unsaturated_tree->Branch("trace_unsaturated", &trace_unsaturated);
            #endif
        #endif

//////////////          TRACES END         //////////////

//////////////          SDEvent         //////////////

    // Useless
    //int removed_stations; tree->Branch("removed_stations", &removed_stations);
    //int accidental_stations; tree->Branch("accidental_stations", &accidental_stations);
    //int candidate_stations; tree->Branch("candidate_stations", &candidate_stations);

//////////////          SDEvent END        //////////////

//////////////          LDF         //////////////

    //Those commented always return 0
    float ldf_beta; tree->Branch("ldf_beta", &ldf_beta); 
    //double ldf_beta_delta; tree->Branch("ldf_beta_delta", &ldf_beta_delta); 
    float ldf_beta_sys; tree->Branch("ldf_beta_sys", &ldf_beta_sys); 

    float ldf_gamma; tree->Branch("ldf_gamma", &ldf_gamma);
    //double ldf_gamma_error; tree->Branch("ldf_gamma_error", &ldf_gamma_error);

    float ldf_likelihood; tree->Branch("ldf_likelihood", &ldf_likelihood);
    // double ldf_status; tree->Branch("ldf_status", &ldf_status); //Always 4.5 or 4.501 with no apparent reason

    // Useless
    //double ldf_chi2; tree->Branch("ldf_chi2", &ldf_chi2);
    //double ldf_ndof; tree->Branch("ldf_ndof", &ldf_ndof);

    //double ldf_nkg_fermi_mu; tree->Branch("ldf_nkg_fermi_mu", &ldf_nkg_fermi_mu);
    //double ldf_nkg_fermi_tau; tree->Branch("ldf_nkg_fermi_tau", &ldf_nkg_fermi_tau);
     
    //double ldf_ref_distance; tree->Branch("ldf_ref_distance", &ldf_ref_distance); //returns always 1000
    
    float ldf_shower_size; tree->Branch("ldf_shower_size", &ldf_shower_size);
    float ldf_shower_size_error; tree->Branch("ldf_shower_size_error", &ldf_shower_size_error);
    //double ldf_shower_size_sys; tree->Branch("ldf_shower_size_sys", &ldf_shower_size_sys);

    //double ldf_size_geomagnetic_corr; tree->Branch("ldf_size_geomagnetic_corr", &ldf_size_geomagnetic_corr);
    //double ldf_size_geomagnetic_corr_error; tree->Branch("ldf_size_geomagnetic_corr_error", &ldf_size_geomagnetic_corr_error);

    //double ldf_size_weather_corr; tree->Branch("ldf_size_weather_corr", &ldf_size_weather_corr);
    //double ldf_size_weather_corr_error; tree->Branch("ldf_size_weather_corr_error", &ldf_size_weather_corr_error);

//////////////         LDF END     //////////////

//////////////          SdFootprintData         //////////////
// Everything returns 0
     
    //double length; tree->Branch("length", &length);
    //double width; tree->Branch("width", &width);
    //double speed; tree->Branch("speed", &speed);
    //double speed_std; tree->Branch("speed_std", &speed_std);
    //double tot_fraction; tree->Branch("tot_fraction", &tot_fraction);
    //double area_over_peak_asymmetry; tree->Branch("area_over_peak_asymmetry", &area_over_peak_asymmetry);
    //int alignment; tree->Branch("alignment", &alignment);

//////////////          SdFootprintData END         //////////////

//////////////          SdRiseTime         //////////////
//Everything returns 0

    //double risetime_alpha; tree->Branch("risetime_alpha", &risetime_alpha);
    //double risetime_beta; tree->Branch("risetime_beta", &risetime_beta);
    //double risetime_1000; tree->Branch("risetime_1000", &risetime_1000);
    //double risetime_1000_error; tree->Branch("risetime_1000_error", &risetime_1000_error);
    //double risetime_chi2; tree->Branch("risetime_chi2", &risetime_chi2);
    //double risetime_ndf; tree->Branch("risetime_ndf", &risetime_ndf);
    //double risetime_xmax; tree->Branch("risetime_xmax", &risetime_xmax);
    //double risetime_xmax_error_down; tree->Branch("risetime_xmax_error_down", &risetime_xmax_error_down);
    //double risetime_xmax_error_up; tree->Branch("risetime_xmax_error_up", &risetime_xmax_error_up);

//////////////          SdRiseTime END         //////////////

//////////////          SDRecShower         //////////////
// Commented values return 0 or always the same value

    // Useless
    //double angle_chi2; tree->Branch("angle_chi2", &angle_chi2);
    //double angle_dof; tree->Branch("angle_dof", &angle_dof);
    
    // This is pretty much the same as axis_site
    //double axis_core_x, axis_core_y, axis_core_z;
    //tree->Branch("axis_core_x", &axis_core_x);
    //tree->Branch("axis_core_y", &axis_core_y);
    //tree->Branch("axis_core_z", &axis_core_z);

    float axis_site_x, axis_site_y, axis_site_z;
    tree->Branch("axis_site_x", &axis_site_x);
    tree->Branch("axis_site_y", &axis_site_y);
    tree->Branch("axis_site_z", &axis_site_z);

    float azimuth; tree->Branch("azimuth", &azimuth);
    float azimuth_error; tree->Branch("azimuth_error", &azimuth_error);
   
    //double beta; tree->Branch("beta", &beta);
    //double beta_error; tree->Branch("beta_error", &beta_error);
    
    // Useless
    //double curvature; tree->Branch("curvature", &curvature);
    //double curvature_error; tree->Branch("curvature_error", &curvature_error);

    //double curvature_offset; tree->Branch("curvature_offset", &curvature_offset);
    //double curvature_offset_error; tree->Branch("curvature_offset_error", &curvature_offset_error);

    float core_easting_error; tree->Branch("core_easting_error", &core_easting_error);
    float core_northing_easting_corr; tree->Branch("core_northing_easting_corr", &core_northing_easting_corr);
    float core_northing_error; tree->Branch("core_northing_error", &core_northing_error);
    float core_site_x, core_site_y, core_site_z;
    tree->Branch("core_site_x", &core_site_x);
    tree->Branch("core_site_y", &core_site_y);
    tree->Branch("core_site_z", &core_site_z);

    int core_time_nanosecond; tree->Branch("core_time_nanosecond", &core_time_nanosecond);
    //int core_time_second; tree->Branch("core_time_second", &core_time_second);
  
    // float cos_zenith; tree->Branch("cos_zenith", &cos_zenith);
    // float cos_zenith_error; tree->Branch("cos_zenith_error", &cos_zenith_error);

    float energy; tree->Branch("en", &energy); 
    float energy_error; tree->Branch("en_error", &energy_error); 
    //double energy_ldfsys; tree->Branch("energy_ldfsys", &energy_ldfsys); 
    float energy_total_error; tree->Branch("en_total_error", &energy_total_error); 

    //double gamma; tree->Branch("gamma", &gamma); 
    //double gamma_error; tree->Branch("gamma_error", &gamma_error); 

    bool is5t5; tree->Branch("isftf", &is5t5);
    bool is6t5; tree->Branch("isstf", &is6t5);
    
    float plane_front_axis_x, plane_front_axis_y, plane_front_axis_z;
    tree->Branch("plane_front_axis_x", &plane_front_axis_x);
    tree->Branch("plane_front_axis_y", &plane_front_axis_y);
    tree->Branch("plane_front_axis_z", &plane_front_axis_z);

    //double mpd_max; tree->Branch("mpd_max", &mpd_max);
    //double mpd_min; tree->Branch("mpd_min", &mpd_min);

    float radius_of_curvature; tree->Branch("radius_of_curvature", &radius_of_curvature);
    float radius_of_curvature_error; tree->Branch("radius_of_curvature_error", &radius_of_curvature_error);

    float radius_point_x, radius_point_y, radius_point_z;
    tree->Branch("radius_point_x", &radius_point_x);
    tree->Branch("radius_point_y", &radius_point_y);
    tree->Branch("radius_point_z", &radius_point_z);
    
    //double r_opt; tree->Branch("r_opt", &r_opt);
    //double r_opt_error; tree->Branch("r_opt_error", &r_opt_error);

    float s1000; tree->Branch("s1000", &s1000);
    float s1000_beta_sys; tree->Branch("s1000_beta_sys", &s1000_beta_sys);
    float s1000_error; tree->Branch("s1000_error", &s1000_error);
    float s1000_total_error; tree->Branch("s1000_total_error", &s1000_total_error);

    //double seed_axis_x, seed_axis_y, seed_axis_z;
    //tree->Branch("seed_axis_x", &seed_axis_x);
    //tree->Branch("seed_axis_y", &seed_axis_y);
    //tree->Branch("seed_axis_z", &seed_axis_z);
    
    float shower_size; tree->Branch("shower_size", &shower_size);
    float shower_size_error; tree->Branch("shower_size_error", &shower_size_error);
    //0 for "N19" as label, 1 for "S1000"
    bool shower_size_label; tree->Branch("shower_size_label", &shower_size_label);
    //double shower_size_sys; tree->Branch("shower_size_sys", &shower_size_sys);

    float time_residual_mean; tree->Branch("time_residual_mean", &time_residual_mean);
    float time_residual_spread; tree->Branch("time_residual_spread", &time_residual_spread);

    float zenith; tree->Branch("zen", &zenith);
    float zenith_azimuth_correlation; tree->Branch("zen_azimuth_correlation", &zenith_azimuth_correlation);
    float zenith_error; tree->Branch("zen_error", &zenith_error);

    // WARNING: ZENITH WILL BE TRANSFORMED TO 1 / COS(ZENITH)
    // The error will be the error of the zenith angle

    //bool has_mpd_histogram; tree->Branch("has_mpd_histogram", &has_mpd_histogram);

    float signal_uncertainty; tree->Branch("signal_uncertainty", &signal_uncertainty);


//////////////          SDRecShower END        //////////////

//////////////          SDRecStation         //////////////
 
    // Useless
    //double assym_corr_risetime; tree->Branch("assym_corr_risetime", &assym_corr_risetime);
    //double assym_corr_risetime_error; tree->Branch("assym_corr_risetime_error", &assym_corr_risetime_error);
    //double assym_corr_risetime_error_kg; tree->Branch("assym_corr_risetime_error_kg", &assym_corr_risetime_error_kg);
    //double assym_corr_risetime_kg; tree->Branch("assym_corr_risetime_kg", &assym_corr_risetime_kg);

    float azimuth_sp; tree->Branch("azimuth_sp", &azimuth_sp);
    // These two are valid in simulations but not in data (always zero in data)
    float corr_risetime; tree->Branch("corr_risetime", &corr_risetime);
    float corr_risetime_rms; tree->Branch("corr_risetime_rms", &corr_risetime_rms);
    
    // Useless
    //double curvature_time_residual; tree->Branch("curvature_time_residual", &curvature_time_residual);

    //double electron_signal; tree->Branch("electron_signal", &electron_signal);
    float falltime; tree->Branch("falltime", &falltime);
    float falltime_rms; tree->Branch("falltime_rms", &falltime_rms);

    float ldf_residual; tree->Branch("ldf_residual", &ldf_residual);

    //double muon_component; tree->Branch("muon_component", &muon_component);
    //double muon_signal; tree->Branch("muon_signal", &muon_signal);

    //double photon_signal; tree->Branch("photon_signal", &photon_signal);

    float plane_time_residual; tree->Branch("plane_time_residual", &plane_time_residual);

    //double recovered_signal; tree->Branch("recovered_signal", &recovered_signal);
    //double recovered_signal_error; tree->Branch("recovered_signal_error", &recovered_signal_error);

    //int rejection_status; tree->Branch("rejection_status", &rejection_status);

    float risetime; tree->Branch("risetime", &risetime);
    float risetime_rms; tree->Branch("risetime_rms", &risetime_rms);

    float shape_parameter; tree->Branch("shape_parameter", &shape_parameter);
    float shape_parameter_rms; tree->Branch("shape_parameter_rms", &shape_parameter_rms);

    int signal_end_slot; tree->Branch("signal_end_slot", &signal_end_slot);
    int signal_start_slot; tree->Branch("signal_start_slot", &signal_start_slot);

    float signal_time_sigma_to_intrinsic; tree->Branch("signal_time_sigma_to_intrinsic", &signal_time_sigma_to_intrinsic);

    float sp_distance; tree->Branch("r", &sp_distance);
    float sp_distance_error; tree->Branch("r_error", &sp_distance_error);

    //int t3_error_code; tree->Branch("t3_error_code", &t3_error_code);
    // double t3_window; tree->Branch("t3_window", &t3_window); //weird values (mostly zero)

    float time_50; tree->Branch("time_50", &time_50);
    float time_50_rms; tree->Branch("time_50_rms", &time_50_rms);

    float time_n_second; tree->Branch("time_n_second", &time_n_second);
    //double time_second; tree->Branch("time_second", &time_second);
    float time_variance; tree->Branch("time_variance", &time_variance);

    float total_signal; tree->Branch("s", &total_signal);
    float total_signal_error; tree->Branch("s_error", &total_signal_error);

// CUSTOM
    //0 no saturation, 1 high-gain saturated, 2 low-gain saturated
    int saturated; tree->Branch("saturated", &saturated);

    float area_over_peak; tree->Branch("area_over_peak", &area_over_peak);

    float risetime_gr; tree->Branch("risetime_gr", &risetime_gr);
    float risetime_gr_error; tree->Branch("risetime_gr_error", &risetime_gr_error);
    float risetime_gr_raw; tree->Branch("risetime_gr_raw", &risetime_gr_raw);
    float risetime_gr_raw_low; tree->Branch("risetime_gr_raw_low", &risetime_gr_raw_low);

    float falltime_gr; tree->Branch("falltime_gr", &falltime_gr);

    int trace_length; tree->Branch("trace_length", &trace_length);

//////////////          SDRecStation END        //////////////

//////////////          SIMULATION VARIABLES         //////////////

    #ifdef SIMULATION
    float energy_mc; tree->Branch("en_mc", &energy_mc);
    float zenith_mc; tree->Branch("zen_mc", &zenith_mc);
    // WARNING: energy_mc and zenith_mc will be changed to log10(energy_mc) and 1 / cos(zenith_mc)
    //double declination_mc; tree->Branch("declination_mc", &declination_mc);
    float xmax_mc; tree->Branch("xmax_mc", &xmax_mc);
    float muon_signal_mc; tree->Branch("muon_signal_mc", &muon_signal_mc);
    float electron_signal_mc; tree->Branch("electron_signal_mc", &electron_signal_mc);
    float photon_signal_mc; tree->Branch("photon_signal_mc", &photon_signal_mc);
    float hadron_signal_mc; tree->Branch("hadron_signal_mc", &hadron_signal_mc);
    float unsaturated_total_signal; //tree->Branch("unsaturated_total_signal", &unsaturated_total_signal);
    #endif

//////////////          SIMULATION VARIABLES END         //////////////

//////////////          DATA VARIABLES         //////////////
    #ifndef SIMULATION
    float declination; tree->Branch("declination", &declination);
    float declination_error; tree->Branch("declination_error", &declination_error);
    float right_ascension; tree->Branch("right_ascension", &right_ascension);
    float right_ascension_error; tree->Branch("right_ascension_error", &right_ascension_error);
    //double galactic_latitude; tree->Branch("galactic_latitude", &galactic_latitude);
    //double galactic_latitude_error, tree->Branch("galactic_latitude_error", &galactic_latitude_error);
    //double galactic_longitude; tree->Branch("galactic_longitude", &galactic_longitude);
    //double galactic_longitude_error, tree->Branch("galactic_longitude_error", &galactic_longitude_error);
    int date; tree->Branch("date", &date);
    int time; tree->Branch("time", &time);

    float temperature; tree->Branch("temperature", &temperature);
    //double day_temperature; tree->Branch("day_temperature", &day_temperature);
    float pressure; tree->Branch("pressure", &pressure);
    //double day_pressure; tree->Branch("day_pressure", &day_pressure);
    float humidity; tree->Branch("humidity", &humidity);
    //double day_humidity; tree->Branch("day_humidity", &day_humidity);
    
    bool is_doublet, is_hybrid, is_triplet;
    tree->Branch("is_doublet", &is_doublet);
    tree->Branch("is_hybrid", &is_hybrid);
    tree->Branch("is_triplet", &is_triplet);
    #endif

//////////////          DATA VARIABLES END        //////////////

    //FDRecShower
    float xmax; tree->Branch("xmax", &xmax);
    float fd_energy; tree->Branch("fd_energy", &fd_energy);
    float ecal_energy; tree->Branch("ecal_energy", &ecal_energy);

    //double charge; tree->Branch("charge", &charge);
    //double charge_error; tree->Branch("charge_error", &charge_error);
    //double da_ratio; tree->Branch("da_ratio", &da_ratio);
    //double da_ratio_error; tree->Branch("da_ratio_error", &da_ratio_error);
    //double peak; tree->Branch("peak", &peak);
    //double peak_error; tree->Branch("peak_error", &peak_error);

    for (auto current_file : files)  // mientras haya entradas en 
    {
        std::cout << "Reading " << current_file << "..." << '\n';

        RecEventFile inputFile(current_file.c_str());
        //  inputFile.SetBuffers(&theRecEvent); //cambiar el puntero a un objeto. 
        if(!inputFile.SetBuffers(&(theRecEvent))==RecEventFile::eSuccess)
        {
            std::cout << "==[Bad Buffer Setting]==" << std::endl;
            continue;
        }

        unsigned int ntot=inputFile.GetNEvents();
        if(ntot<=0 || !inputFile.ReadFileInfo(theInfo)==RecEventFile::eSuccess)
        {
            std::cout << "==[ntot<0 OR Wrong Reading]==" << std::endl;
            continue;
        }

        DetectorGeometry detector_geometry;
        inputFile.ReadDetectorGeometry(detector_geometry); 

        // auto startt = std::chrono::high_resolution_clock::now();
        // auto endt = std::chrono::high_resolution_clock::now();
        // using milli = std::chrono::milliseconds;
        while (inputFile.ReadNextEvent() == RecEventFile::eSuccess) 
        {

            // endt = std::chrono::high_resolution_clock::now();
            // std::cout << "The function took " << std::chrono::duration_cast<milli>(endt - startt).count() << std::endl;
            // startt = std::chrono::high_resolution_clock::now();
            // continue;

            #ifdef SIMULATION
            GenShower &gen_shower = theRecEvent->GetGenShower();
            energy_mc = log10(gen_shower.GetEnergy());
            zenith_mc = 1 / cos(gen_shower.GetZenith());
            xmax_mc = gen_shower.GetXmaxInterpolated();
            #endif

            SDEvent &sd_event = theRecEvent->GetSDEvent();
            SdRecShower &sd_rec_shower = sd_event.GetSdRecShower();

            energy = log10(sd_rec_shower.GetEnergy());
            is5t5 = sd_event.Is5T5();
            is6t5 = sd_event.Is6T5();

            #ifdef SIMULATION
            if ((energy_mc < 18.5) and (energy < 18.5)) continue; //1500 m
            // if ((energy_mc > 2e20) and (energy > 2e20)) continue;
            //if ((energy_mc < 1e17) and (energy < 1e17)) continue; //750 m
            #else
            //if (energy < 18.5) continue; //1500 m
            if (energy < 18.4) continue; //1500 m
            //if (energy < 3.16e17) continue; //750 m
            #endif
            //std::cout << "Energy " << energy << "\n";

            std::vector<FDEvent> fd_events = theRecEvent->GetFDEvents();
            xmax = 0.0;
            fd_energy = 0.0;
            ecal_energy = 0.0;
            double xmax_error = 0, energy_error = 0, ecal_error = 0;
            int telescopes = fd_events.size();
            for (int k = 0; k < telescopes; ++k) {
                //std::cout << k << " ";
                auto event = fd_events[k];
                FdRecShower fd_rec_shower = event.GetFdRecShower();
                //std::cout << fd_energy << " " << ecal_energy << std::endl;
                double error = fd_rec_shower.GetXmaxError();
                if (error) {
                    xmax += event.GetFdRecShower().GetXmax() / (error * error);
                    xmax_error += 1 / (error*error);
                }
                error = fd_rec_shower.GetEnergyError();
                if (error) {
	            fd_energy += fd_rec_shower.GetEnergy() / (error * error);
                    energy_error += 1 / (error*error);
                }
                error = fd_rec_shower.GetEcalError();
                if (error) {
                    ecal_energy += fd_rec_shower.GetEcal() / (error * error);
                    ecal_error += 1 / (error*error);
                }
            }
            if (xmax_error) xmax /= xmax_error;
            if (energy_error) fd_energy /= energy_error;
            if (ecal_error) ecal_energy /= ecal_error;

            if (fd_energy) fd_energy = log10(fd_energy);
            
            //std::cout << "FD done\n";

            // if (fd_energy < 3.16e18) continue;

            ++sim_id;

            #ifndef SIMULATION
            Detector &detector = theRecEvent->GetDetector();
            temperature = detector.GetWeatherInformation(eTemperature);
            //day_temperature = detector.GetWeatherInformation(eDayTemperature);
            pressure = detector.GetWeatherInformation(ePressure);
            //day_pressure = detector.GetWeatherInformation(eDayPressure);
            humidity = detector.GetWeatherInformation(eHumidity);
            //day_humidity = detector.GetWeatherInformation(eDayHumidity);
            date = sd_event.GetYYMMDD() + 20000000;
            time = sd_event.GetHHMMSS();
            event_id = sd_event.GetEventId();
            declination = sd_rec_shower.GetDeclination();
            declination_error = sd_rec_shower.GetDeclinationError();
            //galactic_latitude = sd_rec_shower.GetGalacticLatitude();
            //galactic_longitude_error = sd_rec_shower.GetGalacticLatitudeError();
            //galactic_longitude = sd_rec_shower.GetGalacticLongitude();
            //galactic_longitude_error = sd_rec_shower.GetGalacticLongitudeError();
            right_ascension = sd_rec_shower.GetRightAscension();
            right_ascension_error = sd_rec_shower.GetRightAscensionError();
            #endif

//////////////          SDEvent         //////////////

            //removed_stations = sd_event.GetNoOfRemovedStations();
            //accidental_stations = sd_event.GetNoOfAccidentalStations();
            //candidate_stations = sd_event.GetNumberOfCandidates(); 

//////////////          SDEvent END        //////////////

//////////////          LDF         //////////////
            auto ldf = sd_rec_shower.GetLDF();
            ldf_beta = ldf.GetBeta();
            //ldf_beta_delta = ldf.GetBetaError();
            ldf_beta_sys = ldf.GetBetaSys();

            ldf_gamma = ldf.GetGamma();
            //ldf_gamma_error = ldf.GetGammaError();

            ldf_likelihood = ldf.GetLDFLikelihood();
            //ldf_status = ldf.GetLDFStatus();

            //ldf_chi2 = ldf.GetLDFChi2();
            //ldf_ndof = ldf.GetLDFNdof();

            //ldf_nkg_fermi_mu = ldf.GetNKGFermiMu();
            //ldf_nkg_fermi_tau = ldf.GetNKGFermiTau();

            //ldf_ref_distance = ldf.GetReferenceDistance();

            ldf_shower_size = ldf.GetShowerSize();
            ldf_shower_size_error = ldf.GetShowerSizeError();
            //ldf_shower_size_sys = ldf.GetShowerSizeSys();

            //ldf_size_geomagnetic_corr = ldf.GetSizeGeomagneticCorr();
            //ldf_size_geomagnetic_corr_error = ldf.GetSizeGeomagneticCorrError();
            
            //ldf_size_weather_corr = ldf.GetSizeWeatherCorr();
            //ldf_size_weather_corr_error = ldf.GetSizeWeatherCorrError();
//////////////         LDF END     //////////////

//////////////          SdFootprintData         //////////////

            //auto footprint = sd_rec_shower.GetFootprint();
	    //length = footprint.GetLength();
	    //width = footprint.GetWidth();
	    //speed = footprint.GetSpeed();
	    //speed_std = footprint.GetSpeedStandardDeviation();
	    //tot_fraction = footprint.GetTOTFraction();
	    //area_over_peak_asymmetry = footprint.GetAreaOverPeakAsymmetry();
            //alignment = footprint.GetAlignment();

//////////////          SdFootprintData END         //////////////

//////////////          SdRiseTime         //////////////
            //auto sd_risetime = sd_rec_shower.GetRiseTimeResults();
            //risetime_alpha = sd_risetime.GetAlpha(); 
            //risetime_beta = sd_risetime.GetBeta(); 
            //risetime_1000 = sd_risetime.GetRiseTime1000(); 
            //risetime_1000_error = sd_risetime.GetRiseTime1000Error(); 
            //risetime_chi2 = sd_risetime.GetRiseTimeChi2(); 
            //risetime_ndf = sd_risetime.GetRiseTimeNDF(); 
            //risetime_xmax = sd_risetime.GetXmax(); 
            //risetime_xmax_error_down = sd_risetime.GetXmaxErrorDown(); 
            //risetime_xmax_error_up = sd_risetime.GetXmaxErrorUp(); 

//////////////          SdRiseTime END         //////////////

//////////////          SDRecShower         //////////////
	    //angle_chi2 = sd_rec_shower.GetAngleChi2();
	    //angle_dof = sd_rec_shower.GetAngleNDoF();
            //auto axis_core = sd_rec_shower.GetAxisCoreCS();
            //axis_core_x = axis_core[0];
            //axis_core_y = axis_core[1];
            //axis_core_z = axis_core[2];

            auto axis_site = sd_rec_shower.GetAxisSiteCS(); 
            axis_site_x = axis_site[0];
            axis_site_y = axis_site[1];
            axis_site_z = axis_site[2];
              
            //beta = sd_rec_shower.GetBeta();
            //beta_error = sd_rec_shower.GetBetaError();

            azimuth = sd_rec_shower.GetAzimuth();
            azimuth_error = sd_rec_shower.GetAzimuthError(); 

            core_easting_error = sd_rec_shower.GetCoreEastingError();
            core_northing_easting_corr = sd_rec_shower.GetCoreNorthingEastingCorrelation();
            core_northing_error = sd_rec_shower.GetCoreNorthingError();
            auto core_site_cs = sd_rec_shower.GetCoreSiteCS(); 
            core_site_x = core_site_cs[0];
            core_site_y = core_site_cs[1];
            core_site_z = core_site_cs[2];

            core_time_nanosecond = sd_rec_shower.GetCoreTimeNanoSecond();
            //core_time_second = sd_rec_shower.GetCoreTimeSecond();

            //cos_zenith = sd_rec_shower.GetCosZenith();
            //cos_zenith_error = sd_rec_shower.GetCosZenithError();
             
            //curvature = sd_rec_shower.GetCurvature();
            //curvature_error = sd_rec_shower.GetCurvatureError();
            //curvature_offset = sd_rec_shower.GetCurvatureOffset();
            //curvature_offset_delta = sd_rec_shower.GetCurvatureOffsetError();

            energy_error = sd_rec_shower.GetEnergyError();
            //energy_ldfsys = sd_rec_shower.GetEnergyLDFSys();
            energy_total_error = sd_rec_shower.GetEnergyTotalError();
          
            //gamma = sd_rec_shower.GetGamma();
            //gamma_error = sd_rec_shower.GetGammaError();
            
            auto plane_front_axis = sd_rec_shower.GetPlaneFrontAxis(); 
            plane_front_axis_x = plane_front_axis[0];
            plane_front_axis_y = plane_front_axis[1];
            plane_front_axis_z = plane_front_axis[2];

            //auto mpd_data = sd_rec_shower.GetMPDData();
            //if (mpd_data.size()) std::cout << "MPD!!!!" << std::endl;
            //auto mpd_hist = sd_rec_shower.GetMPDHistogram();
            //std::cout << mpd_hist.GetBinContent(0);
            //std::cout << std::endl;

            //mpd_max = sd_rec_shower.GetMPDMax();
            //mpd_min = sd_rec_shower.GetMPDMin();

            radius_of_curvature = sd_rec_shower.GetRadiusOfCurvature();
            radius_of_curvature_error = sd_rec_shower.GetRadiusOfCurvatureError();

            auto radius_point = sd_rec_shower.GetRadiusPoint(); 
            radius_point_x = plane_front_axis[0];
            radius_point_y = plane_front_axis[1];
            radius_point_z = plane_front_axis[2];

	    //r_opt = sd_rec_shower.GetROpt(); 
	    //r_opt_error = sd_rec_shower.GetROptError();
	    
	    s1000 = sd_rec_shower.GetS1000();
	    s1000_beta_sys = sd_rec_shower.GetS1000BetaSys();
	    s1000_error = sd_rec_shower.GetS1000Error();
	    s1000_total_error = sd_rec_shower.GetS1000TotalError();
            
            //auto seed_axis = sd_rec_shower.GetSeedAxis(); 
            //seed_axis_x = seed_axis[0];
            //seed_axis_y = seed_axis[1];
            //seed_axis_z = seed_axis[2];
	    
	    shower_size = sd_rec_shower.GetShowerSize();
	    shower_size_error = sd_rec_shower.GetShowerSizeError();
	    auto label = sd_rec_shower.GetShowerSizeLabel();
            if (strcmp(label, "N19") == 0) shower_size_label = 0;
            else if (strcmp(label, "S1000") == 0) shower_size_label = 1;
            else std::cout << "Another value found:" << label << std::endl;
	    //shower_size_sys = sd_rec_shower.GetShowerSizeSys();

	    time_residual_mean = sd_rec_shower.GetTimeResidualMean();
	    time_residual_spread = sd_rec_shower.GetTimeResidualSpread();

            zenith = 1 / cos(sd_rec_shower.GetZenith());
            zenith_azimuth_correlation = sd_rec_shower.GetZenithAzimuthCorrelation();
            zenith_error = sd_rec_shower.GetZenithError();

	    //has_mpd_histogram = sd_rec_shower.HasMPDHistogram();

	    signal_uncertainty = sd_rec_shower.SignalUncertainty();

//////////////          SDRecShower END         //////////////

            std::vector<SdRecStation> all_stations =
                sd_event.GetStationVector(); //vector de estaciones

            for (auto &station : all_stations) {
                //std::cout <<  "Loop station\n";
                if (not station.IsCandidate()) continue;

//////////////          SDRecStation         //////////////

                //assym_corr_risetime = station.GetAssymCorrRiseTime(zenith);
                //assym_corr_risetime_error = station.GetAssymCorrRiseTimeError(zenith);
                //assym_corr_risetime_error_kg = station.GetAssymCorrRiseTimeErrorKG(zenith);
                //assym_corr_risetime_kg = station.GetAssymCorrRiseTimeKG(zenith);

                azimuth_sp = station.GetAzimuthSP();

                corr_risetime = station.GetCorrRiseTime();
                corr_risetime_rms = station.GetCorrRiseTimeRMS();

                //curvature_time_residual = station.GetCurvatureTimeResidual();

	        //electron_signal = station.GetElectronSignal();

	        falltime = station.GetFallTime();
	        falltime_rms = station.GetFallTimeRMS();

	        ldf_residual = station.GetLDFResidual();

                //auto mpd1 = station.GetMPDDepth();
                //auto mpd2 = station.GetMPDDepthError();
                //auto mpd3 = station.GetMPDSignal();
                //auto mpd4 = station.GetMPDSignalError();
                   
                //muon_component = station.GetMuonComponent();
                //muon_signal = station.GetMuonSignal();
                
                sp_distance = station.GetSPDistance();
                sp_distance_error = station.GetSPDistanceError();

                //photon_signal = station.GetPhotonSignal();

                plane_time_residual = station.GetPlaneTimeResidual();

                //recovered_signal = station.GetRecoveredSignal();
                //recovered_signal_error = station.GetRecoveredSignalError();

                //rejection_status = station.GetRejectionStatus();

                risetime = station.GetRiseTime();
                risetime_rms = station.GetRiseTimeRMS();

                shape_parameter = station.GetShapeParameter();
                shape_parameter_rms = station.GetShapeParameterRMS();

                signal_end_slot = station.GetSignalEndSlot();
                signal_start_slot = station.GetSignalStartSlot();

                signal_time_sigma_to_intrinsic = station.GetSignalTimeSigma2Intrinsic(zenith);

                //t3_error_code = station.GetT3ErrorCode();
                //t3_window = station.GetT3Window();

                time_50 = station.GetTime50();
                time_50_rms = station.GetTime50RMS();

                time_n_second = station.GetTimeNSecond();
                //time_second = station.GetTimeSecond();
                time_variance = station.GetTimeVariance();

                total_signal = station.GetTotalSignal();
                total_signal_error = station.GetTotalSignalError();

// CUSTOM

                saturated = station.IsLowGainSaturated() ?  2 :
                            station.IsHighGainSaturated() ? 1 :
                                                            0;

//////////////          SDRecStation END         //////////////

                risetime_gr_error = risetime_error(sp_distance, zenith, total_signal);

                station_id = station.GetId();
                int start = signal_start_slot;
                int end = signal_end_slot + 1;
                trace_length = end - start;


                //std::cout << std::accumulate(station.GetVEMTrace(1).begin() + start, station.GetVEMTrace(1).begin() + 1 + end, 0.) * station.GetPeak(1) / station.GetCharge(1) + 
                        //std::accumulate(station.GetVEMTrace(2).begin() + start, station.GetVEMTrace(2).begin() + 1 + end, 0.) * station.GetPeak(2) / station.GetCharge(2) +
                        //std::accumulate(station.GetVEMTrace(3).begin() + start, station.GetVEMTrace(3).begin() + 1 + end, 0.) * station.GetPeak(3) / station.GetCharge(3) << " " << total_signal * 3
                        //<< std::endl;

                int counter = 0;
                risetime_gr_raw = 0;
                risetime_gr_raw_low = 0;
                falltime_gr = 0;
                area_over_peak = 0;

                #ifndef SIMULATION
                is_doublet = station.IsDoublet();
                is_hybrid = station.IsHybrid();
                is_triplet = station.IsTriplet();
                #endif

#ifdef SIMULATION 
                // const GenStation* sim_station = sd_event.GetSimStationById(station_id);  //GetMuonTrace, GetElectronTrace, GetMuonSignal, GetElectronSignal are empty or give 0
                //
                // CAVEATS:
                // *One of the components (muon, electron, photon or hadron)
                // can be zero while the others
                // are not, so an independent counter is needed for each component.
                int muon_counter = 0;
                int electron_counter = 0;
                int photon_counter = 0;
                int hadron_counter = 0;
                int unsaturated_counter = 0;
                muon_signal_mc = 0;
                electron_signal_mc = 0;
                photon_signal_mc = 0;
                hadron_signal_mc = 0;
                unsaturated_total_signal = 0;
#endif
                trace.clear();
                trace = std::vector<float>(TRACE_BINS, 0);
                    #ifdef SIMULATION
                    trace_muon.clear();
                    trace_muon = std::vector<float>(TRACE_BINS, 0);
                    trace_electron.clear();
                    trace_electron = std::vector<float>(TRACE_BINS, 0);
                    trace_photon.clear();
                    trace_photon = std::vector<float>(TRACE_BINS, 0);
                        #ifdef UNSATURATED_TRACE
                        trace_unsaturated = std::vector<float>(TRACE_BINS, 0);
                        #endif
                    #endif
                double peak = station.GetPeak(1);
                double charge = station.GetCharge(1);      // For simulations peak and charge always have the same values
                // If peak and charge are zero there will be nans in the trace
                
                for (int pmt = 1; pmt != 4; pmt++) {
                    //std::cout << "Loop PMT\n";
                    std::vector<float> total_trace_signal = station.GetVEMTrace(pmt);
                    //std::cout << "Size " << total_trace_signal.size() << "\n";
                    auto lg = station.GetLowGainTrace(pmt);
		    std::vector<float> nlg(lg.size(), 0);
                    //std::cout << "Size of lg " << lg.size() << "\n";
                    //std::cout << "start and end " << start << " " << end << "\n"; 
		    auto tbaselinelg = station.GetBaselineLG(pmt);
		    auto val = station.GetDynodeAnodeRatio(pmt);
                    // When there is no data available the length
                    // of the trace vector is zero
                    // This happens for the data
                    if (total_trace_signal.size() and std::accumulate(
                                                      total_trace_signal.begin() + start,
                                                      total_trace_signal.begin() + end,
                                                      0.) > 0.1) {
                        //std::cout << "Total signal > threshold\n";
                        ++counter;

                        //risetime, falltime and area over peak
                        std::vector<double> vals =
                            trace_info(total_trace_signal, start, end);

                        risetime_gr_raw += vals[0];
                        //if (saturated == 1) std::cout << "total:" << vals[0] << std::endl;
                        falltime_gr += vals[1];
                        area_over_peak += vals[2];

                        if (lg.size()) {
			for (int ind=start; ind!=end; ++ind)
			    nlg[ind] = (lg[ind]- tbaselinelg) * val / charge ;
                        vals = trace_info(nlg, start, end);
                        risetime_gr_raw_low += vals[0];
                        }
 
                        //if (saturated == 1) std::cout << "low:" << vals[0] << std::endl;

                        for (int ind=start; ind != end; ++ind) {  // Loop for all bins to scale the trace
                            total_trace_signal[ind] *= peak / charge;
                            if (ind < start + TRACE_BINS)
                            trace[ind-start] += total_trace_signal[ind];
                        }
                        // std::cout << "Here" << std::endl;
                        // std::cout << trace_pmt[pmt-1].size() << std::endl;
                        std::fill(trace_pmt[pmt-1].begin(), trace_pmt[pmt-1].end(), 0.);
                        for (int ind=start; ind != std::min(end, start + TRACE_BINS); ++ind)
                            trace_pmt[pmt-1][ind-start] = total_trace_signal[ind];
                        // std::cout << "Done" << std::endl;
                    }

        #ifdef UNSATURATED_TRACE
        if (saturated == 1) {
           std::cout << event_id << std::endl;
           auto tbaseline = station.GetBaseline(pmt);
           auto tbaselinelg = station.GetBaselineLG(pmt);
           auto val = station.GetDynodeAnodeRatio(pmt);
           std::cout << "Baseline: " << tbaseline << std::endl;
           std::cout << "Baseline (low gain channel): " << tbaselinelg << std::endl;
           std::cout << "peak and charge " << peak << " " << charge << std::endl;
           auto lg = station.GetLowGainTrace(pmt);
           auto hg = station.GetHighGainTrace(pmt);
           std::vector<float> nhg(TRACE_BINS, 0);
           std::vector<float> nlg(TRACE_BINS, 0);
           for (int ind=start; ind!=end; ++ind)
              nhg[ind-start] = (hg[ind]- tbaseline) / charge;
           for (int ind=start; ind!=end; ++ind)
              //nlg[ind] = (lg[ind]-tbaselinelg) / charge;
              nlg[ind-start] = (lg[ind]- tbaselinelg) * val / charge ;
           for (auto it=nlg.begin(); it != nlg.begin()+std::min(end-start,TRACE_BINS); ++it)
               std::cout << (*it) << " ";
           std::cout << std::endl;
           std::cout << std::endl;
           for (auto it=nhg.begin(); it != nhg.begin()+std::min(end-start,TRACE_BINS); ++it)
               std::cout << (*it) << " ";
           std::cout << std::endl;
           std::cout << std::endl;
           for (auto it=total_trace_signal.begin()+start; it != total_trace_signal.begin()+start+std::min(end-start,TRACE_BINS); ++it)
               std::cout << (*it) << " ";
           std::cout << std::endl;
           std::cout << std::endl;
           }
           #endif

#ifdef SIMULATION
/*
This is the part where the different component of the traces are computed
Following the advice by Darko, each bin of each component is
obtained by dividing the value for the component by the sum of all the
components and multiplying by the total signal 
*/
        std::vector<float> total_sim_trace(trace_length, 0);
        std::vector<float> muon_signal = station.GetPMTTraces(eMuonTrace, pmt).GetVEMComponent();
        std::vector<float> electron_signal = station.GetPMTTraces(eElectronTrace, pmt).GetVEMComponent();
        std::vector<float> photon_signal = station.GetPMTTraces(ePhotonTrace, pmt).GetVEMComponent();
        std::vector<float> hadron_signal = station.GetPMTTraces(eHadronTrace, pmt).GetVEMComponent();

        for (int ind=start; ind != end; ++ind) {
            if (muon_signal.size() > ind) total_sim_trace[ind-start] += muon_signal[ind];
            if (electron_signal.size() > ind) total_sim_trace[ind-start] += electron_signal[ind];
            if (photon_signal.size() > ind) total_sim_trace[ind-start] += photon_signal[ind];
            if (hadron_signal.size() > ind) total_sim_trace[ind-start] += hadron_signal[ind];
            if ((fabs(total_sim_trace[ind-start]) > 1e-5) and (total_trace_signal.size() > ind)) {
              if (muon_signal.size() > ind)  {
                // std::cout << "Before: " << muon_signal[ind] << std::endl;
                muon_signal[ind] = muon_signal[ind] / total_sim_trace[ind-start] * total_trace_signal[ind];
                // std::cout << "After: "<< muon_signal[ind] << std::endl;
              }
                if (electron_signal.size() > ind) electron_signal[ind] = electron_signal[ind] / total_sim_trace[ind-start] * total_trace_signal[ind];
                if (photon_signal.size() > ind)   photon_signal[ind] = photon_signal[ind] / total_sim_trace[ind-start] * total_trace_signal[ind];
                if (hadron_signal.size() > ind)   hadron_signal[ind] = hadron_signal[ind] / total_sim_trace[ind-start] * total_trace_signal[ind];
            }
            else {
                if (muon_signal.size() > ind) muon_signal[ind] = 0;
                if (electron_signal.size() > ind) electron_signal[ind] = 0;
                if (photon_signal.size() > ind) photon_signal[ind] = 0;
                if (hadron_signal.size() > ind) hadron_signal[ind] = 0;
            }
            if (ind < start+TRACE_BINS) {
                if (muon_signal.size() > ind) trace_muon[ind-start] += muon_signal[ind];
                if (electron_signal.size() > ind) trace_electron[ind-start] += electron_signal[ind];
                if (photon_signal.size() > ind) trace_photon[ind-start] += photon_signal[ind];
            }
        }

        std::fill(trace_muon_pmt[pmt-1].begin(), trace_muon_pmt[pmt-1].end(), 0);
        if (muon_signal.size()) {
            muon_counter++;
            muon_signal_mc += std::accumulate(muon_signal.begin() + start, muon_signal.begin() + end, 0.);
            for (int ind=start; ind != std::min(end, start + TRACE_BINS); ++ind)
                trace_muon_pmt[pmt-1][ind-start] = muon_signal[ind];
        }
        //std::vector<float> electron_signal = sim_station->GetElectronTrace(pmt); //It's always empty
        std::fill(trace_electromagnetic_pmt[pmt-1].begin(), trace_electromagnetic_pmt[pmt-1].end(), 0);
        if (electron_signal.size()) {
            electron_counter++;
            electron_signal_mc += std::accumulate(electron_signal.begin() + start, electron_signal.begin() + end, 0.);
            for (int ind=start; ind != std::min(end, start + TRACE_BINS); ++ind)
                trace_electromagnetic_pmt[pmt-1][ind-start] += electron_signal[ind];
        }
        if (photon_signal.size()) {
            photon_counter++;
            photon_signal_mc += std::accumulate(photon_signal.begin() + start, photon_signal.begin() + end, 0.);
            for (int ind=start; ind != std::min(end, start + TRACE_BINS); ++ind)
                trace_electromagnetic_pmt[pmt-1][ind-start] += photon_signal[ind];
        }
        if (hadron_signal.size()) {
            hadron_counter++;
            hadron_signal_mc += std::accumulate(hadron_signal.begin() + start, hadron_signal.begin() + end, 0.);
        }
        //std::vector<float> unsaturated_signal = station.GetPMTTraces(eTotalUnsaturatedTrace, pmt).GetVEMComponent();
        //if (unsaturated_signal.size()) {
        //    unsaturated_counter++;
        //    unsaturated_total_signal += std::accumulate(unsaturated_signal.begin() + start,
        //            unsaturated_signal.begin() + end, 0.);
        //}

        #ifdef UNSATURATED_TRACE
        if (saturated < 0) {
        auto unsaturated_trace = station.GetPMTTraces(eTotalUnsaturatedTrace, pmt);
        //auto high_gain_unsaturated = station.GetPMTTraces(eTotalUnsaturatedTrace, pmt).GetVEMComponent();
        auto high_gain_unsaturated = station.GetHighGainTrace(pmt);
        auto vem_component = unsaturated_trace.GetLowGainComponent();
        for (auto it=vem_component.begin() + start; it != vem_component.begin() + end; ++it)
            std::cout << *it * peak / charge << " ";
        std::cout << std::endl;
        std::cout << std::endl;
        for (auto it=total_trace_signal.begin() + start; it != total_trace_signal.begin() + end; ++it) 
            std::cout << *it << " ";
        std::cout << std::endl;
        for (auto it=high_gain_unsaturated.begin() + start; it != high_gain_unsaturated.begin() + end; ++it)
            std::cout << *it << " ";
        std::cout << std::endl;
        double baseline = unsaturated_trace.GetBaseline();
        auto test = station.GetPMTTraces(eTotalUnsaturatedTrace, pmt).GetVEMComponent();
        //std::cout << baseline << std::endl;
        std::cout << saturated << std::endl;
        std::cout << high_gain_unsaturated.size() << " " << test.size() << std::endl;
        //if (high_gain_unsaturated.size() == 0) continue;
        for (int ind=start; ind!=std::min(end, start+TRACE_BINS); ++ind) 
            trace_unsaturated[ind-start] += (high_gain_unsaturated[ind] - baseline) * peak / charge;
        std::cout << total_signal << std::endl;
        std::cin.ignore();
        }
        #endif

        //if (unsaturated_signal.size()) {
            //double tmp = std::accumulate(unsaturated_signal.begin() + start, unsaturated_signal.begin() + end, 0.) *
                //station.GetPeak(pmt) / station.GetCharge(pmt);
            //unsaturated_total_signal += tmp;
            //unsaturated_counter++;
        //}
#endif
                 } //PMT loop
                trace_tree_pmt->Fill();

                 risetime_gr = risetime_gr_raw + counter * correction(zenith, azimuth_sp, sp_distance);
                 if (counter > 1) {
                     risetime_gr_raw /= counter;
                     risetime_gr /= counter;
                     risetime_gr_raw_low /= counter;
                     falltime_gr /= counter;
                     area_over_peak /= counter;
                     for (int ind=0; ind!=TRACE_BINS; ++ind) trace[ind] /= counter;
                 }

                #ifdef SIMULATION
                trace_muon_tree_pmt->Fill(); //Out of the if so that the tree is always filled
                trace_electromagnetic_tree_pmt->Fill();

                if (muon_counter > 1) {
                    muon_signal_mc /= muon_counter;
                    for (int ind=0; ind!=TRACE_BINS; ++ind) trace_muon[ind] /= muon_counter; 
                }
                if (electron_counter > 1) {
                    electron_signal_mc /= electron_counter;
                    for (int ind=0; ind!=TRACE_BINS; ++ind) trace_electron[ind] /= electron_counter; 
                }
                if (photon_counter > 1) {
                    photon_signal_mc /= photon_counter;
                    for (int ind=0; ind!=TRACE_BINS; ++ind) trace_photon[ind] /= photon_counter; 
                }
                if (hadron_counter > 1) hadron_signal_mc /= hadron_counter; 
                trace_electromagnetic = trace_photon;
                for (int ind=0; ind!=TRACE_BINS; ++ind)
                    trace_electromagnetic[ind] += trace_electron[ind];
                if (unsaturated_counter > 1) {
                    unsaturated_total_signal /= unsaturated_counter;
                    #ifdef UNSATURATED_TRACE
                    for (int ind=0; ind!=TRACE_BINS; ++ind) trace_unsaturated[ind] /= unsaturated_counter; 
                    #endif
                }
                #endif

                tree->Fill();
                trace_tree->Fill();
    #ifdef SIMULATION
                trace_muon_tree->Fill();
                trace_electromagnetic_tree->Fill();
        #ifdef UNSATURATED_TRACE
                trace_unsaturated_tree->Fill();
        #endif
    #endif
                // if (sim_id > (7.05e6 + 10)) {
		//    main_file->Write();
		//    trace_file->Write();
                //    trace_file_pmt->Write();
		//    trace_muon_file->Write();
                //    trace_muon_file_pmt->Write();
		//    trace_electromagnetic_file->Write();
                //    trace_electromagnetic_file_pmt->Write();
                //    // trace_unsaturated_file->Write();
                //    return;
                // }
            }
        } 
    }

    main_file->Write();
    trace_file->Write();
    trace_file_pmt->Write();
    #ifdef SIMULATION
        trace_muon_file->Write();
        trace_muon_file_pmt->Write();
        trace_electromagnetic_file->Write();
        trace_electromagnetic_file_pmt->Write();
        #ifdef UNSATURATED_TRACE
            trace_unsaturated_file->Write();
        #endif
    #endif
}

int main() {
    std::vector<std::string> files;
    std::string name, line, output;
    int sim_id;
    while(std::getline(std::cin, line) and not line.empty()) {
        std::cout << "Starting loop" << std::endl;
        name = line;
        std::getline(std::cin, line);
        sim_id = std::stoi(line);
        std::getline(std::cin, line);
        output = line;
        while(std::getline(std::cin, line) and not line.empty())
            files.push_back(line);
        output = "Output/" + output + "/";
        make_tree(name, files, output, sim_id);
    }
    return 0;
}
