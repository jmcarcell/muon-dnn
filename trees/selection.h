//C++
#include<algorithm>
#include <iostream>
#include <iomanip>
#include <fstream>
//#include <cstdlib>
#include <string>
//#include <sstream>
//#include <initializer_list>
//#include <map>
#include <numeric>

//ROOT
#include <TROOT.h>
#include <TFile.h>
#include <TTree.h>
//#include <TF1.h>
//#include <TRandom3.h>
//#include <TGraph.h>
//#include <TGraphErrors.h>
//#include <TH1F.h>

//ADST
#include <RecEventFile.h>
#include <RecEvent.h>
#include <SDEvent.h>
#include <GenShower.h>
#include <EventInfo.h>
#include <DetectorGeometry.h>
#include <Detector.h>
#include <LDF.h>
//#include <Traces.h>

//BOOST
//#include <boost/regex.hpp>
